# General tips for writing papers

* [Awesome resources for better writing of CV papers](https://github.com/hassony2/useful-computer-vision-phd-resources/blob/master/Awesome-resources-for-better-writing-of-computer-vision-papers.md)
* [Andrey Karpathy's Blog Post on PhD](http://karpathy.github.io/2016/09/07/phd/)
* [Video on how to write a great research paper](https://www.youtube.com/watch?v=VK51E3gHENc&feature=emb_title)
  1. Don't wait, write: paper-driven research
  2. Identify your key idea
  3. Tell a story
  4. Nail your contributions
  5. Related work to be done later (after detailed description of key idea)
  6. Put your readers first (make the text easy to follow)
  7. Listen to your readers (friends, experts, reviewers)

## Anonymous #1

* The method section is the most important
* The introduction must contain:
  * 1$ on contextualization
  * 1$ summary of existing approaches
  * 1 bullet point contributions list
* The related work section must be maximum 1 page long (use LaTeX paragraph command)
* In total introduction + related work must be approximately 2 pages long (preferably less)
* The method section can be 3/4 pages long:
  * Focus on clarity
  * Spend time working out the relevant notations
* The experiments section must contain a very clear table
  * Use online latex table editor
* You should reach the desired length (8 page for ICCV) a week ahead of the deadline
* Use graphs to highlight where your approach shines
* Try using tikz/matcha to create vectorized figure that need to be as clear as possible

## Anonymous #2

0. The goal is to "chew" the work for the reviewers
1. Introduction
  * What is the problem? Why is it a problem? The motivation
  * Summarize the existing approaches and state why they are not sufficient
  * Explain what you bring to the table while not omitting the methods you are (re-)using
  * Contributions bullet points list
2. Do not hesitate to cite papers! Dilute the other papers criticism as much as possible
3. Related work
  * Contextualize the paper
  * Highlight key differences with different work. The goal is to prevent the potential criticisms.
4. Be very clear
  * Explain the concepts / variables explicitely
  * The paper must be self-contained
  * Do not rely on using the appendix too much!
  * Be precise but do not get lost in details
5. The overall quality must be flawless
  * Proper English
  * Vectorized figures (see .pdf vs .png)
  * Add statistic uncertainty
  * Look up tikz and/or specialized LaTeX package for CNN architecture
6. The contributions must be seen as original
  * It should not look incremental
  * Insist on the novelty
  * Do not use "we combine" or "we improve" too much
7. Try the following exercise: write the paper with only the idea and the experimental protocol.
  * Anticipate ablation study
  * Formulate hypothesis and see how the experiments designed answer them
8. Do not hesitate discussing the experimental results
  * Identify anomalies
  * Discuss to prevent potential criticisms from reviewers
9. IMPORTANT: Do not write bullshit. Either check or delete
10. Start writing as early as possible
  
  
## Anonymous #3

0. Start writing early
1. Breadth first > depth first
  * Organize the paper first (abstract, intro, related work, method, experiments)
  * Start by bullet points and dummy tables into relevant sections
  * Add comments on the results
  * Iterate on the different sections
